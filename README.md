# Simulated X-ray system

## Description
A simulated X-ray system that can be used to generate relative X-ray images of drawn 2D phantoms. The system consists of creating a phantom, using MEE and the Beer-Lambert law to simulate the X-ray attenuation, adding source and detector blur, and then generating the X-ray image with a specified fluence. 

## Usage
Adjust parameters, draw phantom, run code, and see how your x-ray image develops!

## Authors and acknowledgment
Lilly Chiavetta, Ashley Browne, Natalia Roman, Amanda Rosen

Thank you Dr. Libby for teaching us how to model x-ray systems in python.
